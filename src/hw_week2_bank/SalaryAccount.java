package hw_week2_bank;

import java.util.Calendar;

/*
 * A Salary account is exactly similar to a savings 
 * account except that it has a time limit in which 
 * at least one transaction should have taken place. 
 * If no transactions happen on a salary account 
 * for two months (for simulation purposes, consider the limit to be 5 minutes), 
 * the account is frozen and the account holder is notified. 
 */
public class SalaryAccount extends BaseAccount{

	public SalaryAccount(accountType accountType, String owner, int minBal) {
		super(accountType, owner, minBal, 0, 0.0);
		// TODO Auto-generated constructor stub
	}
	

	@Override

	public void openAccount(BaseAccount account) {
		if (accounts.size()>=10) {
			System.err.println("Cannot open more than 10 accounts");
			System.exit(0);
		} else if (account.getBalance() < 3000) {
			System.err.println("Initial balance must be at least 3,000");
			System.exit(0);
		} else {
			accounts.add(account);
			System.out.println("Opened account " +account.getAcctNumber()+" for " +account.getOwner()+"\n");
		}
	}

}
