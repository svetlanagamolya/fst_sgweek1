package hw_week2_bank;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

public abstract class BaseAccount {


	public enum accountType{
		CURRENT,
		LOAN,
		SALARY,
		SAVINGS

	}
	static ArrayList<BaseAccount> accounts = new ArrayList<BaseAccount>(); 
	private int balance;
	private int overdaftMax;
	private double interest;
	private int acctNumber;
	private String owner = "";
	//Instant createdDateTime;
	private accountType accType;
	private Calendar timeStampNow;
	private Calendar lastDepositDate;





	public BaseAccount() {

	}

	public BaseAccount( accountType accountType, String acctHolder, int initalBal, int overdaftMaxAmount, double interestRate) {
		Random rnd = new Random();
		this.accType = accountType;
		this.owner = acctHolder;
		this.balance = initalBal;
		this.acctNumber = rnd.nextInt(255);
		this.overdaftMax = overdaftMaxAmount;
		this.interest = interestRate;
		//this.createdDateTime= Instant.now();
		this.timeStampNow = Calendar.getInstance();
		this.lastDepositDate = Calendar.getInstance();
		//java.time.Clock.systemUTC().instant();

	}
	
	public void whoAmI() {
		System.out.println("BaseAccount");
	}

	public void deposit(BaseAccount account, int amount) {
		if (amount<=0) {         
			System.out.println("Amount to be deposited should be positive");        
		} else {   
			int oldBalance = account.getBalance();
			int newBalance = oldBalance+amount;
			account.setBalance(newBalance);
			account.setLastDepositDate(Calendar.getInstance());
			System.out.println("Amount deposited successfully to Acct "+ account.getAcctNumber()+" of " +account.getOwner());
			System.out.println("Old Balance: "+ oldBalance+"; New Balance: " +account.getBalance()+"\n");
		}         

	}

	public void withdraw(int amount) {
		if (amount<=0){                
			System.out.println("Amount to be withdrawn should be positive"); 
		} 
		else {  
			if (balance < amount) {  
				System.out.println("Insufficient balance");
			} else {  
				balance = balance - amount;
				System.out.println("Amount Withdrawn successfully");
			}
		}

	}

	public Calendar getTimeStampNow() {
		return timeStampNow;
	}
	public void setTimeStampNow(Calendar timeStampNow) {
		this.timeStampNow = timeStampNow;
	}

	public int getOverdaftMax() {
		return overdaftMax;
	}

	public void setOverdaftMax(int overdaftMax) {
		this.overdaftMax = overdaftMax;
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}

	public int getAcctNumber() {
		return acctNumber;
	}

	public void setAcctNumber(int acctNumber) {
		this.acctNumber = acctNumber;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}


	public accountType getAccType() {
		return accType;
	}

	public void setAccType(accountType accType) {
		this.accType = accType;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public int getBalance() {
		return balance;
	}

	public Calendar getLastDepositDate() {
		return lastDepositDate;
	}
	public void setLastDepositDate(Calendar lastDepositDate) {
		this.lastDepositDate = lastDepositDate;
	}
	public void openAccount(BaseAccount account) {
		accounts.add(account);
	}

}
