package hw_week2_bank;



/* A current account does not have any such facilities or restrictions.
 *  The current account allows the customer 
 *  to have an overdraft facility, 
 *  where the customer will have a maximum limit set 
 *  that he can withdraw over the current balance. 
 * 
 */
public class CurrentAccount extends BaseAccount{

	public CurrentAccount(accountType accountType, String owner, int overdraftMax) {
		super(accountType, owner, 0, overdraftMax, 0.0);
		// TODO Auto-generated constructor stub
	}
	
	public CurrentAccount() {
		// TODO Auto-generated constructor stub
	}

	@Override

	public void openAccount(BaseAccount account) {
		if (accounts.size()>=10) {
			System.err.println("Cannot open more than 10 accounts");
			System.exit(0);
		} else if (account.getOverdaftMax() <=0) {
			System.err.println("Set overdraft!");
			System.exit(0);
		} else {
			accounts.add(account);
		}
	}
	
	public void whoAmI() {
		System.out.println("CurrentAccount");
	}

}
