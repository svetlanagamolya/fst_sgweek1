package hw_week2_bank;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
/*
 * XYZ banks wants to automate the basic functionality of one branch. The features that need to be automated are: 
Over the counter activities (deposit, withdraw): 
Account lifecycle: 
Interest Calculation: 
End of day report for daily transactions 
The bank has different type of accounts: 
Savings Account 
Salary Account 
Current Account 
Loan Account 
A savings account is typically used by an individual for personal usage 
and it is necessary for the account to have an average minimum balance of 10000 rs. 
across the quarter.0 

A Salary account is exactly similar to a savings account except that it has 
a time limit in which at least one transaction should have taken place. 
If no transactions happen on a salary account for two months (for simulation purposes, 
consider the limit to be 5 minutes), the account is frozen and the account holder is notified. 

A current account does not have any such facilities or restrictions. 
The current account allows the customer to have an overdraft facility, 
where the customer will have a maximum limit set that he can withdraw over the current balance. 

A loan account can keep track of the amount of loan repaid by a specific customer 
who has taken a loan from the bank. It acts as a standard savings account, 
but has an initial balance of negative (loan amount), and interest is also calculated accordingly. 


Note: Please create a choice based implementation for the requirements shown. Currently assume there will be maximum 10 accounts created.  Please use the following two lines of code to accept data from user. 

Scanner sc=new Scanner(System.in); 

Int choice=sc.nextInt(); // nextFloat, next, nextLine are variations for other data types 
 */
import java.util.List;
import java.util.Scanner;

import hw_week2_bank.BaseAccount.accountType;

public class BranchA {


public String testReflectionAPI = "tApi";

	public static void deposit(int amount) {

	}

	public void withdraw(int amount) {

	}

	public void calculateInterest(int amount, Date startDate, Date endDate) {

	}

	public void endOfDayReport() {

	}

	public static void openAccount(int acctType) {
		Scanner in = new Scanner(System.in);

		try {
			switch (acctType) {
			case 1: //SAVINGS
				System.out.println("********");
				System.out.println("Enter initial balance greater than 10,000");
				int initialBalSAV = in.nextInt();
				in.nextLine(); //workaround to add after nextInt - since it doesn't read newline - Enter
				System.out.println("********");
				System.out.println("Enter Account Holder Name");
				String acctSAVHolder = in.nextLine();
				SavingAcct savAcct = new SavingAcct(accountType.SAVINGS, acctSAVHolder, initialBalSAV);
				savAcct.openAccount(savAcct);
				break;

			case 2: //"SALARY":
				System.out.println("********");
				System.out.println("Enter Account Holder Name");
				String acctSALHolder = in.nextLine();
				System.out.println("Make inital deposit");
				int initialBalSALARY = in.nextInt();
				SalaryAccount salaryAcct = new SalaryAccount(accountType.SAVINGS, acctSALHolder, initialBalSALARY);
				salaryAcct.openAccount(salaryAcct);

				break;

			case 3:// "CURRENT":
				System.out.println("********");
				System.out.println("Enter initial balance greater than 10,000");
				int initialBalCURRENT = in.nextInt();
				in.nextLine(); //workaround to add after nextInt - since it doesn't read newline - Enter
				System.out.println("Enter account Holder Name");
				String acctCURHolder = in.nextLine();
				System.out.println("Enter Overdraft");
				int overdraft = in.nextInt();
				CurrentAccount currentAcct = new CurrentAccount(accountType.CURRENT, acctCURHolder, overdraft);
				currentAcct.openAccount(currentAcct);
				break;

			case 4: //"LOAN":
				System.out.println("********");
				System.out.println("Enter initial balance - negative");
				int initialBalLOAN = in.nextInt();
				in.nextLine(); //workaround to add after nextInt - since it doesn't read newline - Enter
				System.out.println("Enter interest");
				double interest = in.nextInt();
				in.nextLine(); //workaround to add after nextInt - since it doesn't read newline - Enter
				System.out.println("Enter account Holder Name");
				String acctLOANHolder = in.nextLine();
				LoanAccount loanAcct = new LoanAccount(accountType.LOAN, acctLOANHolder, initialBalLOAN, interest);
				loanAcct.openAccount(loanAcct);
				break;

			default:


			}
		} catch (StringIndexOutOfBoundsException e) {
			System.err.println("Code must be exactly 2 chars");
		} 


	}
	private  static int selectOptions() {
		Scanner in = new Scanner(System.in);
		System.out.println("1) Open a new bank account");
		System.out.println("2) Deposit to a bank account");
		System.out.println("3) Withdraw to bank account");
		System.out.println("4) Print account balance");
		System.out.println("5) Quit");
		int userInput = in.nextInt();
		return userInput;

	}
	private static void runBankingMenu(int userInput) {
		Scanner in = new Scanner(System.in);
		String accountHolder;
		int accountType;
		int depositAmt;
		int accNumber;
		int withdrawAmt;
		if (userInput == 1) {
			System.out.println("Select Account type "
					+ "1 = SAVINGS , "
					+ "2 = SALARY , "
					+ "3 = LOAN , "
					+ "4 = CURRENT");
			accountType = in.nextInt();
			openAccount(accountType);
		} else if (userInput ==2 ) {
			System.out.println("Available accounts");
			for (BaseAccount account : BaseAccount.accounts) {
				System.out.println(account.getAcctNumber());			
			}  
			System.out.println("Deposit Enter account Number");
			//Temp printing all accts in array

			accNumber = in.nextInt();
			System.out.println("Deposit Enter Amount");
			depositAmt = in.nextInt();
			System.out.println("Depositing ...");
			makeDeposit(accNumber, depositAmt);
		} else if (userInput ==3 ) {
			System.out.println("ADD Withdraw");
		} else if (userInput ==4 ) {
			System.out.println("ADD Print balance");
		} else if (userInput ==5 ) {
			System.out.println("ADD Print balance");
		} else {
			System.err.println("No such option, exiting");
			System.exit(1);
		}
		in.close();
	}

	private static void refreshAccounts() {
		long lastDepositDate;
		long dateNow = Calendar.getInstance().getTimeInMillis();


		/* 
		 * Exception in thread "main" java.util.ConcurrentModificationException
	at java.base/java.util.ArrayList$Itr.checkForComodification(ArrayList.java:1042)
	at java.base/java.util.ArrayList$Itr.next(ArrayList.java:996)

		 */
		//iterate through clone, remove from the original list
		for (BaseAccount account : new ArrayList<BaseAccount>(BaseAccount.accounts)) {
			lastDepositDate = account.getLastDepositDate().getTimeInMillis();
			if ((account.getAccType() == accountType.SALARY) && (dateNow-lastDepositDate)>=0.5*60*1000){
				BaseAccount.accounts.remove(account);
				System.out.println(account.getAcctNumber()+" to "+account.getOwner()+" has been REMOVED due to inactivity"+"\n");
			}
		}  

	}

	private static void makeDeposit (int acctNumber, int amount) {
		SavingAcct savAcct = new SavingAcct();
		for (BaseAccount account : new ArrayList<BaseAccount>(BaseAccount.accounts)) {
			if (account.getAcctNumber()==acctNumber) {
				savAcct.deposit(account, amount);
			}
		}

	}

	private static void printBalance () {

		for (BaseAccount account : BaseAccount.accounts) {

			int acctNumber = account.getAcctNumber();
			int balance = account.getBalance();
			String owner = account.getOwner();
			System.out.println(acctNumber+" has balance of "+balance+" to "+owner+"\n");

		}
	}

	public static void main(String[] args) throws InterruptedException{
		/*
		 * navigating back and forth through Menu - in progress
		 * 
		 */

		//adding few accounts to test refreshAccounts
		SavingAcct savAcct = new SavingAcct(accountType.SAVINGS, "owner1_SAV", 20000);
		savAcct.openAccount(savAcct);

		SalaryAccount salaryAcct = new SalaryAccount(accountType.SALARY, "owner1_SAL", 5000);
		salaryAcct.openAccount(salaryAcct);

		SalaryAccount salaryAcct1 = new SalaryAccount(accountType.SALARY, "owner2_SAL", 15000);
		salaryAcct.openAccount(salaryAcct1);

		makeDeposit(salaryAcct.getAcctNumber(), 6000);
		System.out.println("***** Print accounts before refresh");
		printBalance();

		Thread.sleep(60000);

		//runBankingMenu(selectOptions()) ;
		refreshAccounts();
		printBalance();


	}


}
