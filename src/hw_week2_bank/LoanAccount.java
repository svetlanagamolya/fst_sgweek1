package hw_week2_bank;



public class LoanAccount extends BaseAccount{

	public LoanAccount(accountType accountType, String owner, int loanBalance, double interestRate) {
		super(accountType, owner, loanBalance, 0, 2.7);
		// TODO Auto-generated constructor stub
	}
	
	public LoanAccount() {
		// TODO Auto-generated constructor stub
	}

	@Override

	public void openAccount(BaseAccount account) {
		if (accounts.size()>=10) {
			System.err.println("Cannot open more than 10 accounts");
			System.exit(0);
		} else if (account.getBalance() >=0) {
			System.err.println("Balance must be negative for Loan account");
			System.exit(0);
		} else {
			accounts.add(account);
		}
	}


}
