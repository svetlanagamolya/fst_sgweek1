package hw_week2_bank;

import java.util.ArrayList;
import java.util.Random;

import hw_week2_bank.BaseAccount.accountType;


/*
 * A savings account is typically used by an individual for personal usage 
 * and it is necessary for the account to have an average minimum balance 
 * of 10000 rs. across the quarter. 
 */
public class SavingAcct extends BaseAccount{

	
	public SavingAcct() {
		super();

	}
	public SavingAcct(accountType accountType, String owner, int minBal) {
		super(accountType, owner, minBal, 0, 0.0);

	}



	@Override
	public void openAccount(BaseAccount account) {
		if (accounts.size()>=10) {
			System.err.println("Cannot open more than 10 accounts");
			System.exit(0);
		} else if (account.getBalance() < 10000) {
			System.err.println("Initial balance must be at least 10,000");
			System.exit(0);
		} else {
			accounts.add(account);
			System.out.println("Opened account " +account.getAcctNumber()+" for " +account.getOwner()+"\n");
		}
	}

}



