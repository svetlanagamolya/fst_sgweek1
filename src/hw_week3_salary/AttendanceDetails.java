package hw_week3_salary;

public class AttendanceDetails {
	private String month;
	private int year;
	private int DaysAvailabale;
	private int DaysPresent;
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getDaysAvailabale() {
		return DaysAvailabale;
	}
	public void setDaysAvailabale(int daysAvailabale) {
		DaysAvailabale = daysAvailabale;
	}
	public int getDaysPresent() {
		return DaysPresent;
	}
	public void setDaysPresent(int daysPresent) {
		DaysPresent = daysPresent;
	}
	public AttendanceDetails(String month, int year, int daysAvailabale, int daysPresent) {
		super();
		this.month = month;
		this.year = year;
		DaysAvailabale = daysAvailabale;
		DaysPresent = daysPresent;
	}
	
	

}
