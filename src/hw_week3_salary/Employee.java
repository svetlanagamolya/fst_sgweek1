package hw_week3_salary;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Scanner;

public abstract class Employee {


	int empId;
	String empName;
	String designation;
	AttendanceDetails AttendanceDet[] = new AttendanceDetails[12];


	static ArrayList<AttendanceDetails> attendance = new ArrayList<AttendanceDetails>();
	public Employee() {

	}

	public Employee(int empId, String empName, String designation) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.designation = designation;

	}

	public String getEmplCode() {
		String emplCode = null;
		String emplLine = null;
		String[] splitEmplLine = null;
		try (FileInputStream fis = new FileInputStream("Employee.txt")){

			Scanner sc = new Scanner(fis); 
			while (sc.hasNextLine())  
				emplLine = sc.nextLine(); 
			splitEmplLine = emplLine.split(",");
		} catch (Exception e) {
			e.printStackTrace();
		} 
		emplCode = splitEmplLine[0];
		return emplCode;
	}


	public AttendanceDetails readAttendace () {
		getEmplCode();


		return null;

	}

	public int getEmpId() {
		return empId;
	}


	public void setEmpId(int empId) {
		this.empId = empId;
	}


	public String getEmpName() {
		return empName;
	}


	public void setEmpName(String empName) {
		this.empName = empName;
	}


	public String getDesignation() {
		return designation;
	}


	public void setDesignation(String designation) {
		this.designation = designation;
	}


	public AttendanceDetails[] getAttendanceDet() {
		return AttendanceDet;
	}


	public void setAttendanceDet(AttendanceDetails[] attendanceDet) {
		AttendanceDet = attendanceDet;
	}



}
