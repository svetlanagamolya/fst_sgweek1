package hw_week3_salary;

import java.util.ArrayList;

public class Developer extends Employee{

	public String expertise;
	
	public Developer() {
		// TODO Auto-generated constructor stub
	}
	
	public Developer(String expertise) {
		super();
		this.expertise = expertise;
	}
	
	public Developer(int empId, String empName, String designation, String expertise) {
		super( empId, empName, designation);
		this.expertise = expertise;
	}

	public String getExpertise() {
		return expertise;
	}

	public void setExpertise(String expertise) {
		this.expertise = expertise;
	}
	
	@Override
	public void getAttendance() {
		attendance.add(null);
		
	}

}
