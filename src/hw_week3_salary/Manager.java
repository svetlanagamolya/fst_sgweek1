package hw_week3_salary;

public class Manager extends Developer {

	public int teamSize;
	
	public int getTeamSize() {
		return teamSize;
	}

	public void setTeamSize(int teamSize) {
		this.teamSize = teamSize;
	}

	public Manager() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Manager(int teamSize) {
		super();
		this.teamSize = teamSize;
		// TODO Auto-generated constructor stub
	}

	public Manager(int empId, String empName, String designation, int teamSize) {
		super(empId, empName, designation, designation);
		this.teamSize = teamSize;
	}

}
