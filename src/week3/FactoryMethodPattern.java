package week3;

import hw_week2_bank.BaseAccount;
import hw_week2_bank.CurrentAccount;

public class FactoryMethodPattern {

	public static void main(String[] args) {

		FactoryClass factory = new FactoryClass();
		BaseAccount ca = factory.getCurrentAccountObjects(2);
		ca.getClass();
		ca.whoAmI();

	}

}
