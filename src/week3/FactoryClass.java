package week3;

import hw_week2_bank.BaseAccount;
import hw_week2_bank.CurrentAccount;
import hw_week2_bank.LoanAccount;

public class FactoryClass {

	public BaseAccount  getCurrentAccountObjects(int option)
	{
		if (option==1) {
			return new LoanAccount();
		} else if (option ==2) {
			return new CurrentAccount();
		} else {
		return null;
		}
	}

}
