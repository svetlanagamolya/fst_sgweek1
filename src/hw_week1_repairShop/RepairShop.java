package hw_week1_repairShop;

/**
 * For the given problem statement, identify and encapsulate the entities using Java 

A local vehicle repair shop needs automation to serve customer better. 
The owner would like to keep track of the status of each job taken, whether it is pending, in progress or done. 
The individual customer information also needs to be maintained so as to provide benefits to loyal customers. 
A customer who has been frequenting the shop for last 2 years is eligible to enrolled in the customer benefit program.  
The billing has to be automated, to provide better transparency to the customer, 
an updated inventory status also needs to be made available when required. 
Plus, the owner would also like to have a daily income report to be prepared. 

(Hint: Designing a class / structure is an iterative process, almost no one gets it perfect on first attempt) 
 * @author infoneer31infostretch
 *
 */
public class RepairShop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//1. collect customer info
					//2. if customer loyal -> provide benefits, else skip
		//3. take job. New -> In progress -> Complete
					//4. as soon as job state=Complete, calculate bill (total job cost - customer discount + tax + maintenance fee)
		// 5. Keep inventory -> update
		//6. Income report

	}
	
	public void enterCustomerDetails() {
		
	}

}
