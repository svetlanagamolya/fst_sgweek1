package hw_week1_repairShop;

public class InventoryItem {
	
	
	public InventoryItem(String itemName, itemCategory e, int itemAmount, float itemPrice) {
		
		this.itemName = itemName;
		this.itemAmount = itemAmount;
		this.itemPrice = itemPrice;
		this.category = e;
		
	}
	
	private String itemName;
	private enum itemCategory{
		SHIFTERS,
		BRAKES,
		TIRES,
		WHELLS,
		HANDLE,
		STEMS,
		SADDLES,
		PEDALS,
		FRAMES,
		FORKS,
		SHOCKS,
		CABLES
		
	}
	private itemCategory category;
	
	private int itemAmount;
	private float itemPrice;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getItemAmount() {
		return itemAmount;
	}
	public void setItemAmount(int itemAmount) {
		this.itemAmount = itemAmount;
	}
	public float getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(float itemPrice) {
		this.itemPrice = itemPrice;
	}
	
	
	

}
